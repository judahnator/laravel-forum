<?php

namespace judahnator\LaravelForum\Tests\Models;


use judahnator\LaravelForum\Models\Comment;
use judahnator\LaravelForum\Models\Post;

class CommentsTest extends ModelTestCase
{

    public function testAddingCommentToPost(): void
    {
        /** @var Post $post */
        $post = $this->user->posts()->create(factory(Post::class)->raw());

        $comment = $post->comments()->create(factory(Comment::class)->raw(['author_id' => $this->user->id]));

        $this->assertEquals(1, $post->comments()->count(), 'The posts count was not what was expected.');
        $this->assertInstanceOf(Comment::class, $comment, 'There is something wrong with the comment we created.');
    }

    public function testAddingCommentToComment(): void
    {
        /** @var Post $post */
        $post = $this->user->posts()->create(factory(Post::class)->raw());

        /** @var Comment $comment */
        $rootComment = $post->comments()->create(factory(Comment::class)->raw(['author_id' => $this->user->id]));

        /** @var Comment $childComment */
        $childComment = $rootComment->comments()->create(factory(Comment::class)->raw(['author_id' => $this->user->id]));

        $this->assertEquals(1, $rootComment->comments()->count(), 'The comments comment count is an unexpected value.');
        $this->assertEquals(2, $post->all_comments_count(), 'The post does not see all the nested comments.');
        $this->assertInstanceOf(Comment::class, $childComment, 'There is somethign wrong with the child comment we created.');

    }

}