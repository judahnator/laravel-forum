<?php

use Faker\Generator as Faker;
use judahnator\LaravelForum\Models\Comment;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->paragraph
    ];
});
