<?php

namespace judahnator\LaravelForum\Tests;


use Illuminate\Support\Facades\Schema;

class MigrationsTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadLaravelMigrations();
        $this->loadMigrationsFrom(__DIR__ . '/../../src/Migrations/');
    }

    public function testMigrateUp(): void
    {
        $this->artisan('migrate');
        $this->assertTrue(Schema::hasTable('comments'), 'The comments table was not created.');
        $this->assertTrue(Schema::hasTable('posts'), 'The posts table was not created.');
        $this->assertTrue(Schema::hasTable('topics'), 'The topics table was not created.');
    }

}