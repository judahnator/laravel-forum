<?php

namespace judahnator\LaravelForum\Tests\Models;


use judahnator\LaravelForum\Models\Topic;
use judahnator\LaravelForum\Tests\Helpers\Models\User;

class TopicsTest extends ModelTestCase
{

    public function testTopicRelationships(): void
    {
        $topic = factory(Topic::class)->create();

        $user = factory(User::class)->create();

        $topic->posts()->create([
            'author_id' => $user->id,
            'title' => 'post1 title',
            'content' => 'post1 content'
        ]);
        $topic->posts()->create([
            'author_id' => $user->id,
            'title' => 'post2 title',
            'content' => 'post2 content'
        ]);

        $this->assertEquals(2, $topic->posts()->count(), 'Something went wrong while testing a topics posts.');
    }

}