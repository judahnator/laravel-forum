<?php

namespace judahnator\LaravelForum\Tests\Helpers\Models;


use Illuminate\Database\Eloquent\Model;
use judahnator\LaravelForum\Models\Post;

class User extends Model
{

    public function posts() {
        return $this->hasMany(Post::class, 'author_id');
    }

}