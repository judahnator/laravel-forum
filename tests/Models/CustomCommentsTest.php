<?php

namespace judahnator\LaravelForum\Tests\Models;


use judahnator\LaravelForum\Models\Comment;
use judahnator\LaravelForum\Models\Post;
use judahnator\LaravelForum\Tests\Helpers\Models\Comment as TestComment;

class CustomCommentsTest extends CommentsTest
{

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('laravel-forum.models.comment', TestComment::class);
    }

    public function testCorrectModelBeingUsed(): void
    {
        /** @var Post $post */
        $post = $this->user->posts()->create(factory(Post::class)->raw());

        $comment = $post->comments()->create(factory(Comment::class)->raw(['author_id' => $this->user->id]));

        $this->assertInstanceOf(TestComment::class, $comment);
    }

}