<?php

namespace judahnator\LaravelForum\Tests\Models;


use judahnator\LaravelForum\Models\Post;
use judahnator\LaravelForum\Models\Topic;
use judahnator\LaravelForum\Tests\Helpers\Models\User;

class PostsTest extends ModelTestCase
{

    public function testRelationships(): void
    {
        /** @var Post $post */
        $post = $this->user->posts()->create(factory(Post::class)->raw());

        $post
            ->topic()
            ->associate(
                factory(Topic::class)->create()
            );

        $this->assertEquals(1, $this->user->posts()->count(), 'Cannot find the users post');
        $this->assertInstanceOf(User::class, $post->author);
        $this->assertInstanceOf(Topic::class, $post->topic);
    }

    public function testScopes(): void
    {
        // Delete all posts and topics so our count isn't off
        Post::truncate();
        Topic::truncate();

        $user = factory(User::class)->create();

        /** @var Topic $topic */
        $topic = factory(Topic::class)->create();

        // Create two posts on this topic
        $topic->posts()->create(factory(Post::class)->raw(['author_id' => $user->id]));
        $topic->posts()->create(factory(Post::class)->raw(['author_id' => $user->id]));

        // Create one post with no topic
        factory(Post::class)->create((['author_id' => $user->id]));

        $this->assertEquals(1, Post::uncategorized()->count(), 'Uncategoriezed posts are not working.');
        $this->assertEquals(2, Post::categoriezed()->count(), 'Categorized posts are not working.');

    }

}