<?php

namespace judahnator\LaravelForum\Traits;


use Illuminate\Support\Collection;
use judahnator\LaravelForum\Models\Comment;

trait HasComments
{

    /**
     * Returns the comments for this item.
     *
     * @return mixed
     */
    public function comments()
    {
        return $this->morphMany(config('laravel-forum.models.comment', Comment::class), 'commentable');
    }

    /**
     * Eager loads all nested comments.
     *
     * @return mixed
     */
    public function all_comments()
    {
        return $this->comments()->with('all_comments');
    }

    /**
     * Returns the total comment count recursively.
     *
     * @return int
     */
    public function all_comments_count(): int
    {
        $total = 0;

        /** @var Collection $comments_to_parse */
        $comments_to_parse = $this->all_comments()->get();

        while ($comments_to_parse->isNotEmpty()) {

            /** @var Comment $current_comment */
            $current_comment = $comments_to_parse->pop();

            if ($current_comment->comments->count()) {
                $comments_to_parse = $comments_to_parse->merge($current_comment->comments);
            }

            $total++;
        }

        return $total;
    }

}