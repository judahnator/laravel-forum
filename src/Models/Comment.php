<?php

namespace judahnator\LaravelForum\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use judahnator\LaravelForum\Traits\HasAuthor;
use judahnator\LaravelForum\Traits\HasComments;

class Comment extends Model
{

    use HasAuthor,
        HasComments,
        SoftDeletes;

    protected $fillable = [
        'author_id',
        'content'
    ];

    public function commentable() {
        $this->morphTo();
    }

    public function getMorphClass()
    {
        return config('laravel-forum.models.comment', Comment::class);
    }

    public function post()
    {
        return $this->belongsTo(config('laravel-forum.models.post', Post::class));
    }

}