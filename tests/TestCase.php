<?php

namespace judahnator\LaravelForum\Tests;


use judahnator\LaravelForum\Tests\Helpers\Models\User;
use Orchestra\Testbench\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('laravel-forum', require __DIR__.'/../src/Config/laravel-forum.php');
        $app['config']->set('laravel-forum.user_model', User::class);

        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

}