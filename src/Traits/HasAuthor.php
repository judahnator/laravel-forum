<?php

namespace judahnator\LaravelForum\Traits;


trait HasAuthor
{

    public function author()
    {
        return $this->belongsTo(config('laravel-forum.user_model'), 'author_id');
    }

}