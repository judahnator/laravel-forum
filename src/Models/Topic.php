<?php

namespace judahnator\LaravelForum\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'title',
        'slug',
        'description'
    ];

    public function posts()
    {
        return $this->hasMany(config('laravel-forum.models.post', Post::class));
    }

}