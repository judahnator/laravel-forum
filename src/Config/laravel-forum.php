<?php

return [

    /*
     * Your applications user model
     */
    'user_model' => \App\User::class,

    /*
     * The application Comment/Post/Topic models.
     * These settings should usually be left alone unless you know what you are doing.
     */
    'models' => [
        'comment' => \judahnator\LaravelForum\Models\Comment::class,
        'post' => \judahnator\LaravelForum\Models\Post::class,
        'topic' => \judahnator\LaravelForum\Models\Topic::class
    ]

];