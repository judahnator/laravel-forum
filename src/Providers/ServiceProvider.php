<?php

namespace judahnator\LaravelForum\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../Migrations/');

        $this->publishes([
            __DIR__.'/../Config/laravel-forum.php' => config_path('laravel-forum.php')
        ], 'config');
    }

}