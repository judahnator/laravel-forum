<?php

namespace judahnator\LaravelForum\Tests\Models;


use judahnator\LaravelForum\Tests\Helpers\Models\User;
use judahnator\LaravelForum\Tests\TestCase;

abstract class ModelTestCase extends TestCase
{

    /** @var User */
    protected $user;

    protected function setUp()
    {
        parent::setUp();

        $this->loadLaravelMigrations();
        $this->loadMigrationsFrom(__DIR__ . '/../../src/Migrations/');
        $this->artisan('migrate');

        $this->withFactories(__DIR__.'/../Helpers/Factories/');

        $this->user = factory(User::class)->create();
    }

}