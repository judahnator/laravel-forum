<?php

use Faker\Generator as Faker;
use judahnator\LaravelForum\Models\Topic;

$factory->define(Topic::class, function (Faker $faker) {
    $title = $faker->sentence;
    return [
        'title' => $title,
        'slug' => str_slug($title),
        'description' => $faker->paragraph,
    ];
});
