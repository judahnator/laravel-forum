<?php

use Faker\Generator as Faker;
use judahnator\LaravelForum\Models\Post;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->paragraph,
    ];
});
