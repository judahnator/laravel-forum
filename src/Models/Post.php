<?php

namespace judahnator\LaravelForum\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use judahnator\LaravelForum\Traits\HasAuthor;
use judahnator\LaravelForum\Traits\HasComments;

class Post extends Model
{

    use HasAuthor,
        HasComments,
        SoftDeletes;

    protected $fillable = [
        'author_id',
        'title',
        'content'
    ];

    public function getMorphClass()
    {
        return config('laravel-forum.models.post', Post::class);
    }

    public function topic()
    {
        return $this->belongsTo(config('laravel-forum.models.topic', Topic::class));
    }

    /**
     * Scope to select posts that belong to a topic.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeCategoriezed(Builder $query): Builder
    {
        return $query->whereHas('topic');
    }

    /**
     * Scope to select posts that do not have a topic.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeUncategorized(Builder $query): Builder
    {
        return $query->whereDoesntHave('topic');
    }

}