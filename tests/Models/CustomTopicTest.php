<?php

namespace judahnator\LaravelForum\Tests\Models;

use judahnator\LaravelForum\Models\Post;
use judahnator\LaravelForum\Models\Topic;
use judahnator\LaravelForum\Tests\Helpers\Models\Topic as TestTopic;

class CustomTopicTest extends TopicsTest
{

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('laravel-forum.models.topic', TestTopic::class);
    }

    public function testCorrectModelBeingUsed(): void
    {
        /** @var TestTopic $topic */
        $topic = TestTopic::create(factory(Topic::class)->raw());

        /** @var Post $post */
        $post = $topic
            ->posts()
            ->create(
                factory(Post::class)
                    ->raw([
                        'author_id' => $this->user->id
                    ])
            );

        $this->assertInstanceOf(
            TestTopic::class,
            $post->topic()->first()
        );
    }

}