<?php

namespace judahnator\LaravelForum\Tests\Models;


use judahnator\LaravelForum\Models\Post;
use judahnator\LaravelForum\Tests\Helpers\Models\Post as TestPost;

class CustomPostsTest extends PostsTest
{

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('laravel-forum.models.post', TestPost::class);
    }

    public function testCorrectModelBeingUsed(): void
    {
        $this->assertInstanceOf(
            Post::class,
            $this->user->posts()->create(factory(Post::class)->raw())
        );
    }

}